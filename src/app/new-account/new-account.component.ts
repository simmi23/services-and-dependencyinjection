import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AccountService} from '../account.service';

@Component({
  selector: 'app-new-account',
  templateUrl: './new-account.component.html',
  styleUrls: ['./new-account.component.css'],
  // providers: [AccountService]
})
export class NewAccountComponent implements OnInit {
  // using Service so @Output Commented
  // @Output() accountAdded = new EventEmitter<{name: string, statusN: string}>();
  constructor(private accountService: AccountService) {
    // subscribe the event emit() from account component(event created in service AccountService)
    this.accountService.statusUpdateAlert.subscribe(
      (status: string) => alert('The service for cross component communivation and its status is ' + status)
    );
  }

  ngOnInit() {
  }

  /*onNewAccountCreated(accountName: string, status: string) {
      this.accountAdded.emit({
          name: accountName,
          statusN: status
        }
      );
  }*/
  // using Service
  onNewAccountCreated(accountName: string, status: string) {
   this.accountService.accountAdded(accountName, status);
  }
}
