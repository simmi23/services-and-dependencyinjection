import {Component, OnInit} from '@angular/core';
import {ConsoleLogService} from './consoleLog.service';
import {Console2Service} from './Console2.service';
import {AccountService} from './account.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [Console2Service]
  // Angular automatically create object we dont have to manually create
 // providers: [Console2Service, AccountService] // adding this providers to appModule as this
  // is at highest level and injecting service nto another is possible at highest Level i.e
  // in AppModule only
})
export class AppComponent implements OnInit{
  // Angular automatically create object we dont have to manually create
  constructor(private consoleSer2: Console2Service, private accountService: AccountService) {}
  title = 'servicesAndDependencyInjection';
  /*accounts = [
    {
      name : 'Master Account',
      statusN : 'active'
    },
    {
      name: 'Test Account',
      statusN: 'inactive'
    },
    {
      name: 'Hidden Account',
      statusN: 'Unknown'
    }
  ];
  onAccountAdded(accountCreated: {name: string; statusN: string}) {
    this.accounts.push(accountCreated);
  }

  onStatusChanged(statusChanged: {id: number; newStatus: string}) {
    this.accounts[statusChanged.id].statusN = statusChanged.newStatus;
    // calling service(console.log written in service and use anywhere in app)
    // Here we are manually creating object
    const serviceUse = new ConsoleLogService();
    serviceUse.onstatuschanged(statusChanged.newStatus);

    // Angular automatically create object we dont have to manually create
    this.consoleSer2.statusCh(statusChanged.newStatus);
  }*/
  accounts: {name: string, statusN: string}[] = [];
  ngOnInit(): void {
    this.accounts = this.accountService.accounts;
  }
}
