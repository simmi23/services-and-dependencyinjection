import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AccountService} from '../account.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css'],
  // removing providers because instance is overwriting from AppComponent
  // so to use same instance of parent i.e. AppComponent
  // providers: [AccountService]
})
export class AccountComponent implements OnInit {
  @Input() account: {name: string, statusN: string};
  @Input() id: number;
  // using service so commented
  // @Output() statusChanged = new EventEmitter<{id: number, newStatus: string}>();
  // keep instance of AccountService in constructor bcoz that tells the angular that we want an instance
  constructor(private accountService: AccountService) { }

  ngOnInit() {
  }
  // using service so commented
  /*onsetTo(status: string) {
    this.statusChanged.emit({
      id: this.id,
      newStatus: status
    });
  }*/
  onsetTo(status: string) {
    this.accountService.statusChanged(this.id, status);
    // emit() the created event using EventEmitter in Service
    this.accountService.statusUpdateAlert.emit(status);
  }
}
