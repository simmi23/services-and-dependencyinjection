import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AccountComponent } from './account/account.component';
import { NewAccountComponent } from './new-account/new-account.component';
import {AccountService} from './account.service';
import {ConsoleLogService} from './consoleLog.service';

@NgModule({
  declarations: [
    AppComponent,
    AccountComponent,
    NewAccountComponent
  ],
  imports: [
    BrowserModule
  ],
  // Higher Level Service
  // the service ConsoleLogService is injected into other service so it has no providers so
  // write it here
  // medium article for injecting one service to other
  // https://medium.com/@MatheusCAS/injecting-a-service-into-another-service-in-angular-3b253df5c21
  providers: [AccountService, ConsoleLogService],
  bootstrap: [AppComponent]
})
export class AppModule { }
