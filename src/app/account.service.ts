// Here to add another service decorator available is @Injectable
// we Add @Injectable in that service where we are injecting other service
// not in the service which we inject

import {EventEmitter, Injectable} from '@angular/core';
import { ConsoleLogService } from './consoleLog.service';
// here we are injecting console service so @Injectable() added here
@Injectable()
export class AccountService {
  accounts = [
    {
      name : 'Master Account',
      statusN : 'active'
    },
    {
      name: 'Test Account',
      statusN: 'inactive'
    },
    {
      name: 'Hidden Account',
      statusN: 'Unknown'
    }
  ];
  // Suppose we want to trigger an event in one component and listen in other component
  // we define an EventEmitter here and emit this event from one component and subscribe from other
  statusUpdateAlert = new EventEmitter<string>();
  constructor(private consoleService: ConsoleLogService) {}
  accountAdded(name1: string, status: string) {
    this.accounts.push({name: name1, statusN: status});
    // taking service from ConsoleLog Service
    this.consoleService.onstatuschanged(status);
  }
  statusChanged(id: number, newStatus: string) {
    this.accounts[id].statusN = newStatus;
  }
}
